# Cluster AKS
This is module is about to create one AKS a cluster Kubernetes on the Azure Microsoft.

## Usage

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Usage

```hcl
provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
}

# Create network
module "azure-base-networks" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/azure/azure-base-networks?ref=v1.0"
  tags   = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Virtual Network Variables ##
  vnet_name    = var.vnet_name
  network_cidr = var.network_cidr

  ## Public Subnet Variables ##
  public_subnets = var.public_subnets

  ## Private Subnet Variables ##
  private_subnets = var.private_subnets

  ## NAT Gateway Variables ##
  public_subnet_gateway_ip = var.public_subnet_gateway_ip
  public_subnet_gateway    = var.public_subnet_gateway
}

# Create AKS Cluster
module "cluster_aks" {

  source = "../.."
  tags   = var.tags
  depends_on = [
    module.azure-base-networks
  ]

  ## Resource Group Variables ##
  resource_group_name = "example_azure_resource_group"
  location            = "francecentral"

  ## Cluster AKS
  cluster_name               = var.cluster_name
  virtual_node_addon_enabled = var.virtual_node_addon_enabled
  subnet_name                = module.azure-base-networks.private_subnets["private_subnet_compute"].name
  kubernetes_version         = var.kubernetes_version
  network_plugin             = var.network_plugin
  outbound_type              = var.outbound_type
  zone                       = var.zone
  auto_scaling_enabled       = var.auto_scaling_enabled
  host_encryption_enabled    = var.host_encryption_enabled
  http_routing_enabled       = var.http_routing_enabled
  agent_type                 = var.agent_type
  disk_type                  = var.disk_type
  subnet_id                  = module.azure-base-networks.private_subnets["private_subnet_compute"].id

  ## Node pool
  node_pool_name = var.node_pool_name
  node_count     = var.node_count
  vm_size        = var.vm_size
  max_count      = var.max_count
  min_count      = var.min_count
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_agent_type"></a> [agent\_type](#input\_agent\_type) | The type of Node Pool which should be created | `string` | `"VirtualMachineScaleSets"` | no |
| <a name="input_auto_scaling_enabled"></a> [auto\_scaling\_enabled](#input\_auto\_scaling\_enabled) | Is the Kubernetes auto scaler enabled | `bool` | `false` | no |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | Cluster name that will be created | `string` | n/a | yes |
| <a name="input_disk_type"></a> [disk\_type](#input\_disk\_type) | The type of disk which should be used for the Operating System | `string` | `"Managed"` | no |
| <a name="input_host_encryption_enabled"></a> [host\_encryption\_enabled](#input\_host\_encryption\_enabled) | Is host encryption enabled | `bool` | n/a | yes |
| <a name="input_http_routing_enabled"></a> [http\_routing\_enabled](#input\_http\_routing\_enabled) | Is HTTP Application Routing Enabled? | `bool` | `true` | no |
| <a name="input_kubernetes_version"></a> [kubernetes\_version](#input\_kubernetes\_version) | the version of Kubernetes that will be used | `string` | `"1.21"` | no |
| <a name="input_location"></a> [location](#input\_location) | Azure region where the resource group will be created | `string` | n/a | yes |
| <a name="input_max_count"></a> [max\_count](#input\_max\_count) | The maximum number of nodes which should exist within the Node Pool | `number` | `10` | no |
| <a name="input_min_count"></a> [min\_count](#input\_min\_count) | The maximum number of nodes which should exist within the Node Pool | `number` | `2` | no |
| <a name="input_network_plugin"></a> [network\_plugin](#input\_network\_plugin) | Network plugin to use for networking | `string` | `"kubenet"` | no |
| <a name="input_node_count"></a> [node\_count](#input\_node\_count) | The initial number of nodes which should exist within the Node Pool | `number` | `3` | no |
| <a name="input_node_pool_name"></a> [node\_pool\_name](#input\_node\_pool\_name) | Name of the Kubernetes Node pool | `string` | `"cluster-node-pool"` | no |
| <a name="input_os_type_node"></a> [os\_type\_node](#input\_os\_type\_node) | The Operating System which should be used for the Node Pool | `string` | `"Linux"` | no |
| <a name="input_outbound_type"></a> [outbound\_type](#input\_outbound\_type) | The egress outbound  routing method which should be used for the Kubernetes Cluster | `string` | `"loadBalancer"` | no |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | Name of the resource group | `string` | n/a | yes |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | The ID of a Subnet where the Kubernetes Node Pool should exist | `string` | n/a | yes |
| <a name="input_subnet_name"></a> [subnet\_name](#input\_subnet\_name) | The subnet name for the virtual nodes to run | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Resources Tags | `map(any)` | n/a | yes |
| <a name="input_type_identity"></a> [type\_identity](#input\_type\_identity) | Identity type | `string` | `"SystemAssigned"` | no |
| <a name="input_virtual_node_addon_enabled"></a> [virtual\_node\_addon\_enabled](#input\_virtual\_node\_addon\_enabled) | Enable virtual node addon | `bool` | `false` | no |
| <a name="input_vm_size"></a> [vm\_size](#input\_vm\_size) | The SKU which used for the Virtual Machines used in the Node Pool | `string` | `"Standard_D2_v2"` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | A list of Availability Zones across which the Node Pool should be spread | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id_cluster"></a> [id\_cluster](#output\_id\_cluster) | The id of the cluster |
| <a name="output_kube_config"></a> [kube\_config](#output\_kube\_config) | Contents of the cluster kube\_config |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
