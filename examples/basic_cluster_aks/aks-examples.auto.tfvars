### Definition of variables
## Cluster AKS
cluster_name               = "clusterHT"
virtual_node_addon_enabled = false
kubernetes_version         = "1.20.9"
network_plugin             = "azure"
outbound_type              = "loadBalancer"
zone                       = ["1", "2"]
auto_scaling_enabled       = false
host_encryption_enabled    = false
agent_type                 = "VirtualMachineScaleSets"
http_routing_enabled       = true
disk_type                  = "Managed"

## Node pool
node_pool_name = "nodepoolht1"
node_count     = 1
vm_size        = "Standard_DS2_v2"
min_count      = 0
max_count      = 0
os_type_node   = "Linux"