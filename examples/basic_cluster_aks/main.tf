provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
}

# Create network
module "azure-base-networks" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/azure/azure-base-networks?ref=v1.0"
  tags   = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Virtual Network Variables ##
  vnet_name    = var.vnet_name
  network_cidr = var.network_cidr

  ## Public Subnet Variables ##
  public_subnets = var.public_subnets

  ## Private Subnet Variables ##
  private_subnets = var.private_subnets

  ## NAT Gateway Variables ##
  public_subnet_gateway_ip = var.public_subnet_gateway_ip
  public_subnet_gateway    = var.public_subnet_gateway
}

# Create AKS Cluster
module "cluster_aks" {

  source = "../.."
  tags   = var.tags
  depends_on = [
    module.azure-base-networks
  ]

  ## Resource Group Variables ##
  resource_group_name = "example_azure_resource_group"
  location            = "francecentral"

  ## Cluster AKS
  cluster_name               = var.cluster_name
  virtual_node_addon_enabled = var.virtual_node_addon_enabled
  subnet_name                = module.azure-base-networks.private_subnets["private_subnet_compute"].name
  kubernetes_version         = var.kubernetes_version
  network_plugin             = var.network_plugin
  outbound_type              = var.outbound_type
  zone                       = var.zone
  auto_scaling_enabled       = var.auto_scaling_enabled
  host_encryption_enabled    = var.host_encryption_enabled
  http_routing_enabled       = var.http_routing_enabled
  agent_type                 = var.agent_type
  disk_type                  = var.disk_type
  subnet_id                  = module.azure-base-networks.private_subnets["private_subnet_compute"].id

  ## Node pool
  node_pool_name = var.node_pool_name
  node_count     = var.node_count
  vm_size        = var.vm_size
  max_count      = var.max_count
  min_count      = var.min_count
}
