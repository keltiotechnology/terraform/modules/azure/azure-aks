## Declaration of the variables
## Cluster AKS
variable "cluster_name" {
  type        = string
  description = "Cluster name that will be created"
}

variable "type_identity" {
  type        = string
  description = "Identity type"
  default     = "SystemAssigned"
}

variable "kubernetes_version" {
  type        = string
  description = "the version of Kubernetes that will be used"
  default     = "1.21"
}

variable "network_plugin" {
  type        = string
  description = " Network plugin to use for networking"
  default     = "kubenet"
}

variable "outbound_type" {
  type        = string
  description = "The egress outbound  routing method which should be used for the Kubernetes Cluster"
  default     = "loadBalancer"
}

variable "zone" {
  type        = list(string)
  description = "A list of Availability Zones across which the Node Pool should be spread"
}

variable "auto_scaling_enabled" {
  type        = bool
  description = "Is the Kubernetes auto scaler enabled"
  default     = false
}

variable "host_encryption_enabled" {
  type        = bool
  description = "Is host encryption enabled"
}

variable "virtual_node_addon_enabled" {
  type        = bool
  description = "Enable virtual node addon "
  default     = true
}

variable "agent_type" {
  type        = string
  description = "The type of Node Pool which should be created"
  default     = "VirtualMachineScaleSets"
}

variable "disk_type" {
  type        = string
  description = "The type of disk which should be used for the Operating System"
  default     = "Managed"
}

variable "http_routing_enabled" {
  type        = bool
  description = "Is HTTP Application Routing Enabled?"
  default     = true
}

variable "os_type_node" {
  type        = string
  description = "The Operating System which should be used for the Node Pool"
  default     = "Linux"
}

variable "node_pool_name" {
  type        = string
  description = "Name of the Kubernetes Node pool"
  default     = "cluster-node-pool"
}

variable "node_count" {
  type        = number
  description = "The initial number of nodes which should exist within the Node Pool"
  default     = 3
}

variable "max_count" {
  type        = number
  description = "The maximum number of nodes which should exist within the Node Pool"
  default     = 10
}

variable "min_count" {
  type        = number
  description = "The maximum number of nodes which should exist within the Node Pool"
  default     = 2
}

variable "vm_size" {
  type        = string
  description = "The SKU which used for the Virtual Machines used in the Node Pool"
  default     = "Standard_D2_v2"
}