## Tags ##
tags = {
  Environment = "testing"
}

## Provider Variables ##
subscription_id = "b15e3114-37af-4463-8659-786dc5cc0bff"

## Resource Group Variables ##
resource_group_name = "example_azure_resource_group"
location            = "francecentral"

## Virtual Network Variables ##
vnet_name    = "azure_net"
network_cidr = "10.0.0.0/8"

## Public Subnet Variables ##
public_subnets = {}

## Private Subnet Variables ##
private_subnets = {
  private_subnet_compute = {
    name                       = "private_subnet_compute"
    address_prefixes           = ["10.2.0.0/16"]
    private_subnet_delegations = {}
  }
}

## NAT Gateway ##
public_subnet_gateway_ip = {
  name          = "public_subnet_ip"
  allocation    = "Static"
  sku           = "Standard"
  prefix_name   = "public_subnet_ip_prefix"
  prefix_length = 30
}

public_subnet_gateway = {
  name    = "public_subnet_gateway"
  sku     = "Standard"
  timeout = 10
}