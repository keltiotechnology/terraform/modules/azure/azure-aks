resource "azurerm_kubernetes_cluster" "cluster_aks" {
  name                = var.cluster_name
  location            = var.location
  resource_group_name = var.resource_group_name
  dns_prefix          = var.cluster_name
  tags                = var.tags
  kubernetes_version  = var.kubernetes_version

  default_node_pool {
    name           = var.sys_nodepool_name
    node_count     = var.sys_nodepool_nodecount
    vm_size        = var.sys_nodepool_vmsize
    type           = var.agent_type
    vnet_subnet_id = var.subnet_id
  }

  identity {
    type = var.type_identity
  }

  network_profile {
    network_plugin = var.network_plugin
    outbound_type  = var.outbound_type
  }

  dynamic "aci_connector_linux" {
    for_each = var.virtual_node_addon_enabled ? { yes : "of course" } : {}
    content {
      subnet_name = var.subnet_name
    }
  }

  http_application_routing_enabled = var.http_routing_enabled

  provisioner "local-exec" {
    command     = "az aks get-credentials --resource-group ${var.resource_group_name} --name ${var.cluster_name}"
    interpreter = ["bash", "-c"]
  }
}

resource "azurerm_kubernetes_cluster_node_pool" "node_pool_cluster" {
  name                   = var.node_pool_name
  kubernetes_cluster_id  = azurerm_kubernetes_cluster.cluster_aks.id
  vm_size                = var.vm_size
  zones                  = var.zones
  enable_auto_scaling    = var.auto_scaling_enabled
  node_count             = var.node_count
  max_count              = var.max_count
  min_count              = var.min_count
  os_type                = var.os_type_node
  enable_host_encryption = var.host_encryption_enabled
  os_disk_type           = var.disk_type
  tags                   = var.tags

  lifecycle {
    ignore_changes = [vnet_subnet_id]
  }

  depends_on = [azurerm_kubernetes_cluster.cluster_aks]
}
