## Declaration of the variables
## Resource groupe
variable "resource_group_name" {
  type        = string
  description = "Name of the resource group"
}

variable "location" {
  type        = string
  description = "Azure region where the resource group will be created "
}

## Tags
variable "tags" {
  type        = map(any)
  description = "Resources Tags"
}

## Cluster AKS
variable "cluster_name" {
  type        = string
  description = "Cluster name that will be created"
}

variable "type_identity" {
  type        = string
  description = "Identity type"
  default     = "SystemAssigned"
}

variable "kubernetes_version" {
  type        = string
  description = "the version of Kubernetes that will be used"
  default     = "1.21"
}

variable "network_plugin" {
  type        = string
  description = " Network plugin to use for networking"
  default     = "kubenet"
}

variable "outbound_type" {
  type        = string
  description = "The egress outbound  routing method which should be used for the Kubernetes Cluster"
  default     = "loadBalancer"
}

variable "zones" {
  type        = list(string)
  description = "A list of Availability Zones across which the Node Pool should be spread"
}

variable "auto_scaling_enabled" {
  type        = bool
  description = "Is the Kubernetes auto scaler enabled"
  default     = false
}

variable "host_encryption_enabled" {
  type        = bool
  description = "Is host encryption enabled"
}

variable "virtual_node_addon_enabled" {
  type        = bool
  description = "Enable virtual node addon "
  default     = false
}

variable "subnet_name" {
  type        = string
  description = "The subnet name for the virtual nodes to run"
}

variable "http_routing_enabled" {
  type        = bool
  description = "Is HTTP Application Routing Enabled?"
  default     = true
}

variable "agent_type" {
  type        = string
  description = "The type of Node Pool which should be created"
  default     = "VirtualMachineScaleSets"
}

variable "disk_type" {
  type        = string
  description = "The type of disk which should be used for the Operating System"
  default     = "Managed"
}

variable "subnet_id" {
  type        = string
  description = "The ID of a Subnet where the Kubernetes Node Pool should exist"
}

variable "os_type_node" {
  type        = string
  description = "The Operating System which should be used for the Node Pool"
  default     = "Linux"
}

variable "node_pool_name" {
  type        = string
  description = "Name of the Kubernetes Node pool"
  default     = "cluster-node-pool"
}

variable "node_count" {
  type        = number
  description = "The initial number of nodes which should exist within the Node Pool"
  default     = 3
}

variable "max_count" {
  type        = number
  description = "The maximum number of nodes which should exist within the Node Pool"
  default     = 10
}

variable "min_count" {
  type        = number
  description = "The maximum number of nodes which should exist within the Node Pool"
  default     = 2
}

variable "vm_size" {
  type        = string
  description = "The SKU which used for the Virtual Machines used in the Node Pool"
  default     = "Standard_D2_v2"
}

### Default system nodepool settings    ###
### Edit these variables only if needed ###

variable "sys_nodepool_name" {
  type        = string
  description = "The name of the default system nodepool"
  default     = "systemnodes"
}

variable "sys_nodepool_vmsize" {
  type        = string
  description = "The size of the default system nodepool VM: Standard_DS2_v2 or Standard_E2as_v4 recommended"
  default     = "Standard_DS2_v2"
}

variable "sys_nodepool_nodecount" {
  type        = string
  description = "The number of nodes in the default system nodepool"
  default     = "1"
}
